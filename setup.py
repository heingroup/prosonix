import os
import setuptools
from setuptools import setup, find_packages

NAME = 'prosonix'

with open("README.md", "r") as fh:
    long_description = fh.read()

# Load the package's __version__.py module as a dictionary.
here = os.path.abspath(os.path.dirname(__file__))
about = {}
with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)


setup(
    name=NAME,
    version=about['__version__'],
    author='Veronica Lai // Hein Group',
    description='Unofficial Python package to control Prosonix products. We are not affiliated with Prosonix.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/heingroup/prosonix',
    packages=setuptools.find_packages(),
     classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires='>=3.6',
    install_requires=[
        "ftdi_serial",
    ],
)
