# prosonix

Unofficial Python package to control Prosonix products. We are not affiliated with Prosonix.

## Products supported
- Prosonix Sonolab SL10 Sonocrystallizer (230V/110V)
    - https://www.syrris.com/modules/prosonix-sonolab-sl10-sonocrystallizer-230v110v/
